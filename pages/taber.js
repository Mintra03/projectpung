export default                   
                <TabBar
                    marginTop="500"
                    unselectedTintColor="#949494"
                    tintColor="#742688"
                    barTintColor="#f5f5f5"
                    >
                    <TabBar.Item
                        title="Home"
                        icon={<Icon name="home" />}
                        selected={this.state.selectedTab === 'blueTab'}
                        onPress={this.goToMainPage}
                    >
                    </TabBar.Item>

                    <TabBar.Item
                        title="Data"
                        icon={<Icon name="inbox" />}
                        selected={this.state.selectedTab === 'pinkTab'}
                        onPress={this.goToshowListPage}
                    >
                    </TabBar.Item>

                    <TabBar.Item
                        title="Profile"
                        icon={<Icon name="user" />}
                        selected={this.state.selectedTab === 'yellowTab'}
                        onPress={this.goToProfilePage}
                    >
                    </TabBar.Item>
                </TabBar>