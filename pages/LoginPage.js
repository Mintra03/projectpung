import React, { Component } from 'react';
import { StyleSheet, Image, View, ImageBackground, Text, ScrollView } from 'react-native';
import { Button, InputItem, } from '@ant-design/react-native';
import { push } from 'connected-react-router'
import axios from 'axios';
import { connect } from 'react-redux';

class LoginPage extends Component {

    state = {
        email: 'test2@gmail.com',
        password: '123456',
        username: '',
        firstName: '',
        lastName: '',
        isLoading: false
    }

    login = async () => {
        try {
            this.setState({ isLoading: true })
            const res = await axios.post('https://zenon.onthewifi.com/moneyGo/users/login', {
                email: this.state.email,
                password: this.state.password
            })
            const user = res.data.user
            await this.props.userLogin(user)
            this.setState({ isLoading: false })
            await this.props.history.replace('/MainPage')
        } catch (error) {
            this.setState({ isLoading: false })
            const err = error.response.data.errors ? error.response.data.errors : error
            let errorMessage = ''
            if (err.email) errorMessage += '- Email: ' + err.email + '\n'
            if (err.password) errorMessage += '- Password: ' + err.password + '\n'
            Alert.alert('Incorrect Login', errorMessage)
        }
    }

    signup = () => {
        this.props.history.replace('/RegisterPage')
    }

    render() {
        return (
            <ImageBackground source={require('./12.jpg')} style={{ width: '100%', height: '100%' }}>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={[styles.control, styles.center]}>
                        
                        <View style={styles.container} >
                            <View style={styles.content}>
                                <View style={[styles.box1, styles.center]}>
                                <Text style={[styles.headertext, styles.center]}>MONEY...GO </Text>
                                    <Image source={require('./21.png')} style={[styles.logo, styles.center]} />
                                </View>

                                <View style={[styles.box2, styles.center]}>

                                    <Text style='paddingLeft=15' > Email Address </Text>
                                    <View style={[styles.textInput1, styles.center]}>
                                        <InputItem
                                            clear
                                            value={this.state.email}
                                            onChange={value => {
                                                this.setState({
                                                    email: value
                                                });
                                            }}
                                            placeholder='Email'
                                        />
                                    </View>

                                    <Text> Password </Text>
                                    <View style={[styles.textInput1, styles.center]}>
                                        <InputItem
                                            clear
                                            value={this.state.password}
                                            onChangeText={value => {
                                                this.setState({
                                                    password: value
                                                });
                                            }}
                                            placeholder='Password'
                                        />
                                    </View>

                                    <View style={[styles.button, styles.center]} onPress={this.login}>
                                        <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }} 
                                        style={styles.backgroundColor} onPress={this.login}>Login</Button>
                                    </View>
                                    <Text> or </Text>
                                    <View style={[styles.button, styles.center]} onPress={this.signup}>
                                        <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }} 
                                        style={styles.backgroundColor} onPress={this.signup}>Register</Button>
                                    </View>
                                </View>
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </ImageBackground>

        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userLogin: (user) => {
            dispatch({
                type: 'USER_LOGIN',
                user: user
            })
        }
    }
}

export default connect(null, mapDispatchToProps)(LoginPage)

const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20
    },

    container: {
        flex: 1
    },
    content: {
        flex: 1,
        flexDirection: 'column'
    },

    headertext: {
        color: '#8E26B7',
        fontSize: 36,
        fontWeight: 'bold',
        padding: 0.5,
    },

    box1: {
        flex: 1,
        flexDirection: 'column'
    },

    box2: {
        flex: 1,
        flexDirection: 'column',
        margin: 6,
        top: -20,
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center',
    },

    logo: {
        borderRadius: 150,
        width: 260,
        height: 260,
        top: -20,
    },

    textInput1: {
        backgroundColor: '#EAC8F3',
        flex: 1,
        padding: 10,
        margin: 6,
    },

    button: {
        flex: 1,
        margin: 6,
    },
    margin: {
        margin: 10,
    },

    textInput: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingLeft: 100,
        paddingRight: 100,
        backgroundColor: 'white',
    },

    buttonBox: {
        color: 'pink',
    },

    backgroundColor: {
        backgroundColor: '#8613A5',
        borderColor: '#8613A5',
    },

    control: {
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        width: 325,
        height: 720,
        paddingLeft: 60,
        paddingRight: 60,
        margin: 10,
        marginTop: 30,
        marginLeft: 18,
        padding: 12,
    },

});
