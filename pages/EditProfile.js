import React, { Component } from 'react'
import { StyleSheet, ImageBackground, Image, View, Text, TouchableOpacity, Alert } from 'react-native';
import { Icon, Button, InputItem, List, TabBar, Card, SwipeAction, right } from '@ant-design/react-native'
import axios from 'axios';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'


class editProfilePage extends Component {
    state = {
        selectedTab: 'yellowTab',
        items: [],
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        imagePath: '',
        isLoading: false,
    };



    goToMainPage = () => {
        this.props.history.push('/MainPage')
    }

    goToProfilePage = () => {
        this.props.history.push('/ProfilePage')
    }

    goToLoginPage = () => {
        this.props.history.push('/LoginPage')
    }

    goToEditProfilePage = () => {
        this.props.history.push('/editProfile')
    }

    onClickSave = () => {
        const { user } = this.props
        console.log(user.token);
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users',
            method: 'put',
            headers: {
                'Authorization': `Bearer ${user[0].user.token}`
            },
            data: {
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }
        }).then(() => {
            return axios.get('https://zenon.onthewifi.com/ticGo/users', {
                headers: {
                    'Authorization': `Bearer ${user[0].user.token}`
                },
            }
            )
        })
            .then(response => {
                console.log('response', response);

                this.props.saveUserInRedux(response.data.user);
                this.props.history.push('/profile')
            }).catch(e => {
                console.log("error " + e)
            })

    }

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Profile',
            items: [],
            isLoading: true,
            email: '',
            password: '',
            firstname: '',
            lastname: '',
            imagePath: '',
            loadImage: false,

        };
    }

    onChangeTab() {
        this.setState({
            selectedTab: tabName,
        });
    }


    selectImage = () => {
        const { user } = this.props
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else if (response.uri) {
                const formData = new FormData()
                this.setState({ loadImage: true })
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                axios.post('https://zenon.onthewifi.com/ticGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${user[0].user.token}`
                    },
                    onUploadProgress: progressEvent => {

                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image,
                            loadImage: false
                        })
                    })
                    .catch(error => {
                        console.log(error.response)
                    })

            }
        })
    }

    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/moneyGo/users/image', {
            headers: {
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(response => {
                this.setState({
                    firstName: response.data.user.firstname,
                    lastName: response.data.user.lastname,
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => { console.log('data', err) })
            .finally(() => { console.log('Finally') })
    }


    render() {
        const { user } = this.props
        console.log(user)
        console.log(this.state.items);
        console.log('firstname', this.state.firstname);

        return (


            <ImageBackground source={require('./119.jpeg')} style={{ height: 200 }}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image style={styles.avatar} source={require('./120.jpg')} />
                    </View>

                    <Image style={styles.avatar} source={{ uri: this.state.imagePath }} onPress={this.selectImage} />
                    {/* <Image style={styles.avatar} source={require('./120.jpg')} /> */}
                    <TouchableOpacity onPress={this.selectImage} >
                        <Icon name="camera" size="md" color="#8A848D" style={styles.logo} />
                    </TouchableOpacity>

                    {this.state.loadImage === false ? (
                        <Image
                            style={{ width: '100%', height: '100%', borderRadius: 100 }}
                            source={{ uri: this.state.imagePath }}
                        />
                    ) : (
                            <ActivityIndicator size="large" color="#f47373" />

                        )}


                    <View style={styles.body}>
                        <View style={styles.bodyContent}>

                            <Text style={styles.info}> {this.state.email} </Text>
                            <Text style={styles.info}> {this.state.firstname} </Text>
                            <Text style={styles.info}> {this.state.lastname} </Text>
                            <Text style={styles.info}> {this.state.password} </Text>

                            <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }}
                                style={[styles.backgroundColor, styles.botton]} onPress={this.onClickSave}>
                                Save
                            </Button>

                            <Card >
                                <InputItem
                                    style={{ color: 'white' }}
                                    clear
                                    value={this.state.firstName}
                                    onChangeText={value => { this.setState({ firstName: value }) }}
                                    placeholder="First Name"
                                />
                                <InputItem
                                    style={{ color: 'white' }}
                                    clear
                                    value={this.state.lastName}
                                    onChangeText={value => { this.setState({ firstName: value }) }}
                                    placeholder="First Name"
                                />
                                <InputItem
                                    style={{ color: 'white' }}
                                    clear
                                    value={this.state.password}
                                    onChangeText={value => { this.setState({ firstName: value }) }}
                                    placeholder="First Name"
                                />




                            </Card>



                        </View>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}
export default editProfilePage

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: 200,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: 130
    },
    name: {
        fontSize: 22,
        color: "#FFFFFF",
        fontWeight: '600',
    },
    body: {
        marginTop: 40,
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding: 30,
    },
    name: {
        fontSize: 28,
        color: "#696969",
        fontWeight: "600"
    },
    info: {
        fontSize: 14,
        color: "#00BFFF",
        marginTop: 10
    },
    description: {
        fontSize: 16,
        color: "#696969",
        marginTop: 10,
        textAlign: 'center'
    },
    buttonContainer: {
        marginTop: 10,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
        backgroundColor: "#00BFFF",
    },
});
