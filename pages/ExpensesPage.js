import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Modal, TouchableOpacity, Alert, Image, ScrollView, TextInput } from 'react-native';
import { Button, Icon, Flex, Card, SearchBar, InputItem, List } from '@ant-design/react-native';
import { connect } from 'react-redux'


class RevenuePage extends Component {

    state = {
        wallet: '',
        explanation: '',
        location:'',
    }

    goToTopicExpensesPage = () => {
        this.props.history.push('/TopicExpenses')
    }

    goToshowListPage = () => {
        this.props.history.push('/showListPage')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.content]}>
                    <View style={styles.header}>
                        <View style={[styles.boxIcon, styles.center1]}>
                            <Icon
                                name='close'
                                size={30}
                                color='white'
                                onPress={this.goToshowListPage}
                            />
                        </View>

                        <View style={[styles.boxheader, styles.center1]}>
                            <View style={[styles.boxheader1, styles.center1]}>
                                <View style={[styles.boxheader2, styles.center1]}>
                                    <Text style={[styles.headertext, styles.center]}> 25 ก.พ. 2019 </Text>
                                </View>
                            </View>
                        </View>

                        <View style={[styles.boxIcon, styles.center1]}>
                            <Icon
                                style={styles.iconheaderright}
                                name='check'
                                size={25}
                                color='white'
                                onPress={this.goToshowListPage}
                            />
                        </View>
                    </View>


                    <View style={[styles.box1, styles.center]}>
                        <View style={[styles.textInput1, styles.center]}>
                            <Image source={require('./93.png')} style={[styles.logo]} />
                            <InputItem
                                clear
                                value={this.state.wallet}
                                onChange={value => {
                                    this.setState({
                                        wallet: value,
                                    });
                                }}
                                style={styles.text1}
                                placeholder=" 0.00"
                            />
                        </View>
                    </View>


                    <Button style={[styles.box2]} activeStyle={{ backgroundColor: 'white' }} onPress={this.goToTopicIncomePage} >
                        Choose category </Button>
                    <Icon
                        name='right'
                        size={25}
                        color='#D365EF'
                        style={styles.logo3}
                        onPress={this.goToTopicExpensesPage}
                    />
                    <Icon
                        name='codepen-circle'
                        size={45}
                        color='#D6D6D7'
                        style={styles.logo4}
                        onPress={this.goToTopicExpensesPage}
                    />

                    <View style={[styles.box3]}>
                        <Icon
                            name='edit'
                            size={28}
                            color='#D365EF'
                            style={styles.logo5}
                        />
                        <InputItem
                            value={this.state.explanation}
                            onChange={value => {
                                this.setState({
                                    explanation: value,
                                });
                            }}
                            style={styles.text2}
                            placeholder="คำอธิบาย"
                        />
                        <Icon
                            name='camera'
                            size={26}
                            color='#D365EF'
                            style={styles.logo7}
                        />
                    </View>

                    <View style={[styles.box4]}>
                    <Icon
                            name='environment'
                            size={21}
                            color='#D365EF'
                            style={styles.logo6}
                        />
                        <InputItem
                            value={this.state.location}
                            onChange={value => {
                                this.setState({
                                    location: value,
                                });
                            }}
                            style={styles.text3}
                            placeholder="Location"
                        />
                    </View>

                    <Image source={require('./114.png')} style={[styles.logo8]} />

                </View>
            </View>

        );
    }
}

export default RevenuePage

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        paddingVertical: 0,
        backgroundColor: '#F2F1F2',
    },
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#4A1358',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 0.10,
    },
    headertext: {
        color: 'white',
        fontSize: 17,
        fontWeight: 'bold',
        padding: 15,
        marginBottom: 5,
    },
    content: {
        flex: 12,
        backgroundColor: '#F8F5F9',
        flexDirection: 'column',
    },
    center: {
        alignItems: 'center',
    },
    logo1: {
        width: 180,
        height: 150,
        marginTop: 10,
    },
    logo: {
        width: 50,
        height: 50,
        borderRadius: 11,
        marginRight: 210,
        top: -25,
    },
    boxheader: {
        backgroundColor: '#4A1358',
        margin: 3,
        flexDirection: 'row',
        flex: 1,
    },
    boxIcon: {
        backgroundColor: '#4A1358',
        flex: 0.3,
        flexDirection: 'row',
    },
    center1: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    center: {
        alignItems: 'center',
    },
    textInput1: {
        backgroundColor: 'white',
        flex: 0.15,
        padding: 37,
        margin: 20,
        borderRadius: 10,
    },
    boxheader1: {
        width: 150,
        height: 44,
        borderRadius: 12,
        fontSize: 18,
        backgroundColor: 'white',
        color: 'black',
        marginHorizontal: 3,
        marginBottom: 3,
        marginTop: 8,
    },
    boxheader2: {
        width: 146,
        height: 40,
        borderRadius: 12,
        fontSize: 18,
        backgroundColor: '#4A1358',
        color: 'black',
        marginHorizontal: 3,
        marginTop: 0.6,
        paddingLeft: 10,
    },
    box1: {
        flex: 0.21,
        flexDirection: 'column',
        backgroundColor: '#4A1358',
        height: 30,
    },
    text1: {
        color: '#535252',
        fontSize: 23,
        fontWeight: 'bold',
        flexDirection: 'row',
        marginLeft: 150,
        top: -67,
    },
    text2: {
        color: '#535252',
        fontSize: 16,
        fontWeight: 'bold',
        flexDirection: 'row',
        marginLeft: 48,
        top: -38,
    },
    text3: {
        color: '#535252',
        fontSize: 14,
        fontWeight: 'bold',
        flexDirection: 'row',
        marginLeft: 45,
        top: -30,
    },
    logo2: {
        width: 55,
        height: 55,
        marginLeft: 280,
    },
    logo3: {
        width: 55,
        height: 55,
        marginLeft: 310,
        top: -43,
    },
    logo4: {
        marginLeft: 15,
        top: -107,
    },
    logo5: {
        marginLeft: 21,
        marginTop: 10,
    },
    logo6: {
        marginLeft: 26,
        marginTop: 12,
    },
    logo7: {
        marginLeft: 308,
        marginTop: 12,
        top: -85,
    },
    logo8: {
        width: 130,
        height: 130,
        marginLeft: 110,
        marginBottom: -125,
    },
    box2: {
        backgroundColor: 'white',
        flex: 0.11,
        borderRadius: 0,
    },
    box3: {
        backgroundColor: 'white',
        flex: 0.25,
        top: -100,
    },
    box4: {
        backgroundColor: 'white',
        flex: 0.1,
        top: -99,
    },
    
    inbox1: {
        backgroundColor: 'white',
        margin: 1.5,
        width: 160,
        height: 45,
        flexDirection: 'row',
    },

});
