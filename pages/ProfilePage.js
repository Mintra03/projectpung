import React, { Component } from 'react'
import { StyleSheet, ImageBackground, Image, View, Text, TouchableOpacity, Alert, ActivityIndicator } from 'react-native';
import { Icon, Button, InputItem, List, TabBar, Card, SwipeAction, right } from '@ant-design/react-native'
import axios from 'axios';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'


class ProfilePage extends Component {
    state = {
        selectedTab: 'yellowTab',
        items: [],
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        username: '',
        imagePath: '',
        isLoading: false,
    };

    onClickSave = () => {
        const { user } = this.props
        console.log(user.token);


        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users',
            method: 'put',
            headers: {
                'Authorization': `Bearer ${user.token}`
            },
            data: {
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }
        }).then(() => {
            return axios.get('https://zenon.onthewifi.com/ticGo/users', {
                headers: {
                    'Authorization': `Bearer ${user.token}`
                },
            }
            )
        })
            .then(response => {
                console.log('response', response);

                this.props.saveUserInRedux(response.data.user);
                this.props.history.push('/ProfilePage')
            }).catch(e => {
                console.log("error " + e)
            })



    }


    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Profile',
            items: [],
            isLoading: true,
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            imagePath: '',
            loadImage: false,

        };
    }

    onChangeTab() {
        this.setState({
            selectedTab: tabName,
        });
    }


    selectImage = () => {
        const { user } = this.props
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
                if (response.uri) {
                    const formData = new FormData()
                    formData.append('image', {
                        uri: response.uri,
                        name: response.fileName,
                        type: response.type,
                    })
                    this.setState({ isLoading: true })
                    axios.post('https://zenon.onthewifi.com/moneyGo/users/image', formData, {
                        headers: {
                            Authorization: `Bearer ${this.props.user.token}`
                        },
                        onUploadProgress: progressEvent => {
                            console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                        }
                    })
                        .then(response => {
                            this.setState({
                                imagePath: response.data.image,
                                isLoading: false,
                            })
                        })
                        .catch(error => {
                            this.setState({ isLoading: false })
                            console.log(error.response)
                        })
                    // this.setState({
                    //     imageURI: response.uri
                    // })
                }
            })
        }
    
        componentDidMount() {
            this.getImage()
        }
    
        getImage = () => {
            const { user } = this.props
            this.setState({ isLoading: true })
            axios.get('https://zenon.onthewifi.com/moneyGo/users', {
                headers: {
                    Authorization: `Bearer ${this.props.user.token}`
                }
            })
                .then(response => {
                    this.setState({
                        imagePath: response.data.user.image,
                        isLoading: false
                    })
    
    
                })
                .catch(err => {
                    this.setState({ isLoading: false })
                    console.log(err)
                })
                .finally(() => { console.log('Finally') })
        }
    
    goToMainPage = () => {
        this.props.history.push('/MainPage')
    }

    goToProfilePage = () => {
        this.props.history.push('/ProfilePage')
    }

    goToLoginPage = () => {
        this.props.history.push('/LoginPage')
    }

    goToEditProfilePage = () => {
        this.props.history.push('/editProfile')
    }

    render() {
        const { user } = this.props
        console.log(user)
        console.log(this.state.items);
        console.log('firstname', this.state.firstname);

        return (


            <ImageBackground source={require('./119.jpeg')} style={{ height: 200 }}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image style={styles.avatar} source={require('./120.jpg')} />
                    </View>

                    <Image style={styles.avatar} source={{ uri: this.state.imagePath }} onPress={this.selectImage} />
                    {/* <Image style={styles.avatar} source={require('./120.jpg')} /> */}
                    <TouchableOpacity onPress={this.selectImage} >
                        <Icon name="camera" size="md" color="#8A848D" style={styles.logo} />

                        {this.state.loadImage === false ? (
                                        <Image
                                            style={styles.logo}
                                            source={{ uri: this.state.imagePath }}
                                        />
                                    ) : (
                                            <ActivityIndicator size="large"  color='rgba(16,142,233,10.5)'/>

                                        )}

                    </TouchableOpacity>

                    <View style={styles.body}>
                        <View style={styles.bodyContent}>

                            <Text style={styles.info}> {this.state.email} </Text>
                            <Text style={styles.info}> {this.props.user.firstName} </Text>
                            <Text style={styles.info}> {this.props.user.lastName} </Text>
                            <Text style={styles.info}> {this.props.user.password} </Text>

                            <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }}
                                style={[styles.backgroundColor, styles.botton]} onPress={this.goToEditProfilePage}>
                                Edit
                            </Button>

                            <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }}
                                style={[styles.backgroundColor, styles.botton]} onPress={this.goToLoginPage}>
                                Log out
                            </Button>


                            <TabBar
                                unselectedTintColor="#949494"
                                tintColor="#742688"
                                barTintColor="#f5f5f5"
                            >
                                <TabBar.Item
                                    title="Home"
                                    icon={<Icon name="home" />}
                                    selected={this.state.selectedTab === 'blueTab'}
                                    onPress={this.goToMainPage}
                                >
                                </TabBar.Item>

                                <TabBar.Item
                                    title="Data"
                                    icon={<Icon name="inbox" />}
                                    selected={this.state.selectedTab === 'pinkTab'}
                                    onPress={this.goToshowListPage}
                                >
                                </TabBar.Item>

                                <TabBar.Item
                                    title="Profile"
                                    icon={<Icon name="user" />}
                                    selected={this.state.selectedTab === 'yellowTab'}
                                    onPress={this.goToProfilePage}
                                >
                                </TabBar.Item>
                            </TabBar>
                        </View>

                    </View>

                </View>



            </ImageBackground>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(ProfilePage)

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: 200,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: 130
    },
    name: {
        fontSize: 22,
        color: "#FFFFFF",
        fontWeight: '600',
    },
    body: {
        marginTop: 20,
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding: 30,
    },
    name: {
        fontSize: 28,
        color: "#696969",
        fontWeight: "600"
    },
    info: {
        fontSize: 18,
        color: "#00BFFF",
        marginTop: 6,
        marginTop: 6,
        flexDirection: 'column',
    },
    description: {
        fontSize: 16,
        color: "#696969",
        marginTop: 10,
        textAlign: 'center'
    },
    buttonContainer: {
        marginTop: 10,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
        backgroundColor: "#00BFFF",
    },
    box1: {
        backgroundColor: 'white',
        flexDirection: 'column',
        width: 240,
        height: 34,
        marginTop: 16,
    },
    logo4: {
        width: 45,
        height: 45,
        borderRadius: 11,
        marginTop: 2,
        marginLeft: 6,
    },
    text2: {
        color: '#535252',
        fontSize: 14,
        fontWeight: 'bold',
        flexDirection: 'row',
        marginTop: 6,
        marginLeft: 15,
    },
    inbox1: {
        backgroundColor: 'white',
        margin: 1.5,
        width: 160,
        height: 45,
        flexDirection: 'row',
    },
    backgroundColor: {
        backgroundColor: '#8613A5',
        borderColor: '#8613A5',
    },
    botton: {
        marginTop: 28,
    },
    footer: {
        backgroundColor: '#A5F1CD',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 0.1,
        top: 265,
    },
    logo: {
        top: 39,
        marginLeft: 200,
    },

});
